#example: ./create-database-secret.sh | kubectl -n default create -f -
cat <<EOF
apiVersion: v1
kind: Secret
metadata:
  name: clinical-data-service-db
type: Opaque
data:
  POSTGRES_PASSWORD: $(openssl rand -base64 24 | tr -d '\n' | base64)
  CLINICAL_DATA_SERVICE_PASS: $(openssl rand -base64 24 | tr -d '\n' | base64)
  METABASE_CLIENT_PASS: $(openssl rand -base64 24 | tr -d '\n' | base64)
  MB_DB_PASS: $(openssl rand -base64 24 | tr -d '\n' | base64)
EOF
